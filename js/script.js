$('#main_form').submit(function (e) {
  let values = {};
  const form = this;
  $.each($('#main_form').serializeArray(), function (i, field) {
    values[field.name] = field.value;

    if (field.value === '' && field.name !== 'phone_number') {
      $(`#${field.name}`).addClass('not_filled');
      $(`#tooltip_${field.name}`).css('display', 'block');
    } else {
      $(`#${field.name}`).removeClass('not_filled');
      $(`#tooltip_${field.name}`).css('display', 'none');
    }
  });


  if (values['fullname'] !== '' && values['email'] !== '' && values['notes'] !== '') {
    $.each(values, function (i, v) {
      $('#showFormData').append(`<p>${i} : ${v}</p>`);
    });
    e.preventDefault(e);
    setTimeout(function () {
      form.submit();
    }, 2000);
  } else {
    // Prevent form submit
    e.preventDefault(e);
    hasbeensent = true;
  }
});

$('.form1_input').focus(function () {
  $(this).css('background', 'linear-gradient(to right, rgba(32,178,170,1) 0%,rgba(255,255,255,0) 100%)');
});

$('.form1_input').blur(function () {
  $(this).css('background', 'linear-gradient(to right, rgba(0,128,0,1) 0%,rgba(255,255,255,0) 100%)');
});

$('.form1_submit').mousedown(function () {
  $(this).css('background', 'linear-gradient(to right, rgba(32,178,170,1) 0%,rgba(255,255,255,0) 100%)');
});

$('.form1_submit').mouseup(function () {
  $(this).css('background', 'linear-gradient(to right, rgba(0,128,0,1) 0%,rgba(255,255,255,0) 100%)');
});

var wachten;
var hovered_element;
let hasbeensent = false;

$('.form1_input').hover(function () {
  if (!hasbeensent) {
  hovered_element = this;
    wachten = setTimeout(function () {
      $(`#tooltip_${hovered_element.name}`).css('display', 'block');
    }, 750);
  }
  },
  function () {
    if (!hasbeensent) {
    clearTimeout(wachten);
    $(`#tooltip_${hovered_element.name}`).css('display', 'none');
    }
  });