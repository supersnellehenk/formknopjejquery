var evilTimeout;

$('#evil_laugh').hover(function () {
    evilTimeout = setTimeout(function () {
      var top = Math.floor(Math.random() * ($(window).height() - 0 + 1)) - 15;
      var left = Math.floor(Math.random() * ($(window).width() - 0 + 1)) - 40;

      $('#evil_laugh').css({
        'top': `${top}px`,
        'left': `${left}px`
      });
    }, 100);
  },
  function () {
    clearTimeout(evilTimeout);
  }
);

$('button').click(function () {
  $(this).html('¯\\_(ツ)_/¯');
  $(this).css('background', 'buttonface');
  $('body').css('background', 'url(../img/exit.gif) no-repeat');
  setTimeout(function () {
    if (confirm('Skeletor out! Continue?')) {
      $('body').css('background', 'url(../img/tenor.gif) no-repeat');
      $('button').css('background', 'url(../img/tenor.gif) repeat');
      $('button').html('');
    } else {
      $('body').css('background', 'url(../img/leave.jpg) no-repeat');
      $('button').css({
        'position': 'unset',
        'pointer-events': 'none'
      });
    }
  }, 1500);
});